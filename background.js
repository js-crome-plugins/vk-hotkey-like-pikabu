/* eslint-disable key-spacing */
/*global chrome */
const vkTabsIDs = [];
chrome.tabs.onUpdated.addListener((id, changeInfo, tab) => {
	exec(changeInfo, tab);
});

chrome.browserAction.onClicked.addListener((e) => {
	exec(e, e);
});

function exec(inf, tab) {
	if (inf.status === "complete" &&
	(tab.url.indexOf("https://vk.com/feed") + 1 ||
	tab.url.indexOf("https://vk.com/al_feed") + 1)) {
		vkTabsIDs.push(tab.id);
		chrome.tabs.executeScript(tab.id, {
			'file': "integrate.js"
		});
	}
}

chrome.tabs.onRemoved.addListener((id) => {
	if (vkTabsIDs.includes(id)) {
		vkTabsIDs.pop(id);
	}
})


const defaultSettings = {
	"previusPost": 	{"code": "KeyA",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"nextPost": 	{"code": "KeyD",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"UP": 			{"code": "KeyZ",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"DOWN": 		{"code": "KeyC",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"toStart": 		{"code": "KeyX",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"like": 		{"code": "KeyW",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"dislike": 		{"code": "KeyS",
					"ctrlKey": false,
					"altKey": false,
					"shiftKey": false},
	"run": 			{"code":"KeyR",
					"ctrlKey":false,
					"altKey":false,
					"shiftKey": false},
	"full": 		{"code":"KeyF",
					"ctrlKey":false,
					"altKey":false,
					"shiftKey": false},
	"openComment": 	{"code":"KeyE",
					"ctrlKey":false,
					"altKey":false,
					"shiftKey": false},
	"closeComment": {"code":"KeyQ",
					"ctrlKey":false,
					"altKey":false,
					"shiftKey": false}
}

//обработчик сообщений
chrome.runtime.onMessage.addListener((message, sender) => {
	if (message.target === "background") {
		switch (message.command) {
			case "getSettings":
				getSettings(sender.tab && sender.tab.id);
				break;
			case "applySettings":
				chrome.storage.sync.set(message.args, function() {
					mailingSettings();
				});
				break;
			default:
				console.error(message, "command is not find");
		}
	}
});

function mailingSettings() {
	for (const tabId of vkTabsIDs) {
		getSettings(tabId);
	}
}

//функции обработки
function getSettings(tabId) {
	chrome.storage.sync.get(function (r) {
		if (JSON.stringify(r) === '{}') {
			if (tabId) {
				chrome.tabs.sendMessage(
					tabId,
					{
						"command": "getSettings",
						"settings": defaultSettings
					}
				);
			} else {
				chrome.runtime.sendMessage({
						"command": "getSettings",
						"settings": defaultSettings
					});
			}
			chrome.storage.sync.set(defaultSettings);
		} else if (tabId) {
				chrome.tabs.sendMessage(
					tabId,
					{
						"command": "getSettings",
						"settings": r
					}
				);
			} else {
				chrome.runtime.sendMessage({
						"command": "getSettings",
						"settings": r
					});
			}
	// eslint-disable-next-line no-extra-bind
	}.bind(this));
}

// eslint-disable-next-line no-unused-vars
function isYaBrowser() {
	return navigator.userAgent.search(/YaBrowser/) > 0;
}