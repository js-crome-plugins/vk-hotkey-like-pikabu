/*global chrome mainController*/
//TODO find all inputs (поиск в музыке, шаринг поиск друзей и сообщение, фильтры)
class ListenerTwo {
	constructor(name, handler) {
		this.name = name;
		this.handler = function(e) {
			handler(e.detail);
		}
	}
}

class EventController {

	constructor(element = document) {
		this.controllElement = element;
		this.listeners = [];
	}

	run(name, data) {
		const event = new CustomEvent(name, {'detail': data});
		this.controllElement.dispatchEvent(event);
	}

	add(name, handler) {
		if (this.find(name) !== null) {
			console.error("Already exist listener " + name + ".");
			return false;
		}
		const listener = new ListenerTwo(name, handler)
		this.listeners.push(listener);
		this.controllElement.addEventListener(name, listener.handler);
		return true;
	}

	find(name) {
		for (let i = 0; i < this.listeners.length; ++i) {
			const listener = this.listeners[i];
			if (listener.name === name) {
				this.listeners.splice(i, 1);
				return listener;
			}
		}
		return null;
	}

	remove(name) {
		const listener = this.find(name);
		if (listener === null) {
			console.error("Can't remove event listener " + name + ".");
			return false;
		}
		this.controllElement.removeEventListener(listener.name, listener.handler);
		return true;
	}

	clear() {
		for (let i = 0; i < this.listeners.length; ++i) {
			const listener = this.listeners[i];
			this.controllElement.removeEventListener(listener.name, listener.handler);
		}
		this.listeners = [];
	}
}

class MainController {
	constructor() {
		this.settings = {};
		this.intervals = [];
		this.commands = this.initCommmands();
		this.blockLevel = 2;
		this.activeInp = false;
		this.askSettings();
		this.eventController = new EventController();
		MainController.integrateControll();

		//обработчик сообщений
		chrome.runtime.onMessage.addListener((message) => {
			switch (message.command) {
				case "getSettings":
					this.settings = message.settings;
					document.addEventListener('keypress', this.commandController.bind(this));
					break;
				default:
					console.error(message, "command is not find");
			}
		});

		this.eventController.add('changeBlock', (flag) => {
			this.activeInp = flag;
		});
		this.initFindInputHandlers();
	}

	initFindInputHandlers() {
		const findInput = document.querySelector('.text.ts_input');
		if (findInput) {
				findInput.onfocus = () => {
					this.activeInp = true;
				}
				findInput.onblur = () => {
					this.activeInp = false;
				}
		}
	}

	static integrateControll() {
		const script = document.createElement('script');
		const code =
		`
		class ListenerPkb {
			constructor(name, handler) {
				this.name = name;
				this.handler = function(e) {
					handler(e.detail);
				}
			}
		}
		
		class EventController {
		
			constructor(element = document) {
				this.controllElement = element;
				this.listeners = [];
			}
		
			run(name, data) {
				const event = new CustomEvent(name, {'detail': data});
				this.controllElement.dispatchEvent(event);
			}
		
			add(name, handler) {
				if (this.find(name) !== null) {
					console.error("Already exist listener " + name + ".");
					return false;
				}
				const listener = new ListenerPkb(name, handler)
				this.listeners.push(listener);
				this.controllElement.addEventListener(name, listener.handler);
				return true;
			}
		
			find(name) {
				for (let i = 0; i < this.listeners.length; ++i) {
					const listener = this.listeners[i];
					if (listener.name === name) {
						this.listeners.splice(i, 1);
						return listener;
					}
				}
				return null;
			}
		
			remove(name) {
				const listener = this.find(name);
				if (listener === null) {
					console.error("Can't remove event listener " + name + ".");
					return false;
				}
				this.controllElement.removeEventListener(listener.name, listener.handler);
				return true;
			}
		
			clear() {
				for (let i = 0; i < this.listeners.length; ++i) {
					const listener = this.listeners[i];
					this.controllElement.removeEventListener(listener.name, listener.handler);
				}
				this.listeners = [];
			}
		}

		var eventControllerPKB = new EventController();

		Wall.showEditReplyOrigin = Wall.showEditReply;
		Wall.showEditReply = function() {
			const event = arguments[1];
			if (event.type === 'focus') {
				eventControllerPKB.run('changeBlock', true);
				event.target.addEventListener('blur', ()=> {
					eventControllerPKB.run('changeBlock', false);
				},{'once':true});
			}
			Wall.showEditReplyOrigin(...arguments);
		}
		FastChat.togglePeerOrigin = FastChat.togglePeer;
		FastChat.togglePeer = function() {
			eventControllerPKB.run('changeBlock', true);
			return FastChat.togglePeerOrigin(...arguments);
		}
		FastChat.hideChatCtrlOrigin = FastChat.hideChatCtrl;
		FastChat.hideChatCtrl = function() {
			eventControllerPKB.run('changeBlock', false);
			return FastChat.hideChatCtrlOrigin(...arguments);
		}
		`
		script.textContent = code;
		document.body.appendChild(script);
	}

	smoothScrolling(from, targetPos) {
		this.ci();
		// eslint-disable-next-line no-nested-ternary
		let step = Math.abs(targetPos - from.scrollTop) < 20 ? targetPos - from.scrollTop : (from.scrollTop < targetPos ? 20 : -20);
		this.intervals.push(setInterval(
			function() {
				from.scrollTop += step;
				if (from.scrollTop === targetPos || Math.abs(step) === 0) {
					this.ci();
				} else {
					const delta = step > 0 ? (targetPos - from.scrollTop) : (from.scrollTop - targetPos);
					if (delta < 0) {
						this.ci();
						return;
					}
					if (step > 0) {
						step = step === 1 ? 1 : Math.floor(delta / 2);
					} else {
						step = step === -1 ? -1 : Math.floor(delta / 2) * -1;
					}
				}
		}.bind(this),
		30
		));
		document.body.addEventListener("mousewheel", MainController.ci);
		document.body.addEventListener("touchstart", MainController.ci);
	}

	ci() {
		for (const interval of this.intervals) {
			clearInterval(interval);
			document.body.removeEventListener("mousewheel", MainController.ci);
			document.body.removeEventListener("touchstart", MainController.ci)
		}
	}

	initCommmands() {
		return {
			"previusPost": function() {
				const [, prevElem] = this.getNearestPosts();
				if (this.blockLevel < 2) {
					return;
				}
				if (prevElem) {
					this.smoothScrolling(this.scrollingElement, prevElem.offsetTop);
					// this.scrollingElement.scrollTop = prevElem.offsetTop;
				}
			}.bind(this),
			"nextPost": function() {
				const [nextElem] = this.getNearestPosts();
				if (this.blockLevel < 2) {
					return;
				}
				if (nextElem) {
					this.smoothScrolling(this.scrollingElement, nextElem.offsetTop)
					// this.scrollingElement.scrollTop = nextElem.offsetTop;
				}
			}.bind(this),
			"UP": function() {
				if (this.blockLevel < 1) {
					return;
				}
				const {scrollTop} = this.scrollingElement;
				this.smoothScrolling(this.scrollingElement, scrollTop - 100);
				// this.scrollingElement.scrollTop -= 100;
			}.bind(this),
			"DOWN": function() {
				if (this.blockLevel < 1) {
					return;
				}
				const {scrollTop} = this.scrollingElement;
				this.smoothScrolling(this.scrollingElement, scrollTop + 100);
				// this.scrollingElement.scrollTop += 100;
			}.bind(this),
			"toStart": function() {
				const [
						,
						elem,
						isStart
					] = this.getNearestPosts();
					if (this.blockLevel < 1) {
						return;
					}
				if (elem) {
					// eslint-disable-next-line no-nested-ternary
					this.smoothScrolling(this.scrollingElement, this.blockLevel === 1 ? 0 : isStart ? this.scrollingElement.scrollTop : elem.offsetTop);
					// eslint-disable-next-line no-nested-ternary
					// this.scrollingElement.scrollTop = this.blockLevel === 1 ? 0 : isStart ? this.scrollingElement.scrollTop : elem.offsetTop;
				}
			}.bind(this),
			"like": function() {
				this.getNearestPosts();
				if (this.blockLevel < 1) {
					return;
				}
				const likeBtn = this.currentPost.querySelector("a.like_btn.like._like:not(.active)");
				if (likeBtn) {
					likeBtn.click();
				}
			}.bind(this),
			"dislike": function() {
				this.getNearestPosts();
				if (this.blockLevel < 1) {
					return;
				}
				const likeBtn = this.currentPost.querySelector("a.like_btn.like._like.active");
				likeBtn && likeBtn.click();
			}.bind(this),
			"run": function() {
				if (this.blockLevel < 1) {
					return
				}
				const elem = this.blockLevel === 1 ? this.scrollingElement : this.currentPost;
				this.getNearestPosts();
				const mediaElement = elem.querySelector('.post_thumbed_media');
				if (mediaElement) {
					const a = mediaElement.querySelector('a');
					a && a.click();
				}
				const absLink = elem.querySelector('.ads_ad_nested_link');
				absLink && absLink.click();
				const videoPlay = elem.querySelector('.videoplayer_btn_play');
				if (videoPlay) {
					videoPlay.click();
				} else {
					const videoElement = elem.querySelector('.page_post_thumb_video');
					videoElement && videoElement.click();
				}
			}.bind(this),
			"full": function() {
				this.getNearestPosts();
				if (this.blockLevel < 2) {
					return;
				}
				const moreInfo = this.currentPost.querySelector("a.wall_post_more");
				if (moreInfo) {
					moreInfo.click();
				}
			}.bind(this),
			"openComment": function() {
				this.getNearestPosts();
				if (this.blockLevel < 2) {
					return;
				}
				const postElem = this.currentPost.querySelector('._post.post.page_block');
				if (postElem) {
					const script = document.createElement('script');
					const unId = `pica-pica-${Date.now()}`;
					script.id = unId;
					script.textContent = `wall.postClick('${postElem.id.substring(postElem.id.indexOf('-'))}', event);document.getElementById('${unId}').remove()`;
					document.body.appendChild(script);
					setTimeout(() => {
								const elem = this.scrollingElement.querySelector('.wl_replies_block_wrap');
								if (elem && this.blockLevel === 1) {
									this.scrollingElement.scrollTop = elem.offsetTop + 24;
								}
						}, 1000);
				}
			}.bind(this),
			"closeComment": function() {
				this.scrollingElement;
				if (this.blockLevel === 1) {
					const script = document.createElement('script');
					const unId = `pica-pica-${Date.now()}`;
					script.id = unId;
					script.textContent = `if (!wkcur.noClickHide) { wkcur.wkClicked = true; WkView.hide(); };document.getElementById('${unId}').remove()`;
					document.body.appendChild(script);
				}
			}.bind(this)
		}
	}

	getNearestPosts() {
		for (const child of this.feedContainer.children) {
			const delta = this.scrollingElement.scrollTop - child.offsetTop;
			if (delta <= 0) {
				if (delta === 0) {
					this.currentPost = child;
					return [
						child.nextElementSibling,
						child.previousElementSibling,
						true
					];
				}
				this.currentPost = child.previousElementSibling;
				return [
					child,
					child.previousElementSibling,
					false
				];
			}
		}
		this.currentPost = this.feedContainer.lastElementChild;
		return [
			this.feedContainer.lastElementChild,
			this.feedContainer.lastElementChild.previousElementSibling,
			false
		];
	}

	// eslint-disable-next-line class-methods-use-this
	get feedContainer() {
		return document.querySelector('#feed_rows');
	}

	// eslint-disable-next-line class-methods-use-this
	get scrollingElement() {
		const vkLayer = document.getElementById('wk_layer_wrap');
		if (vkLayer && vkLayer.style.display !== 'none') {
			this.blockLevel = this.activeInp ? 0 : 1;
			return vkLayer;
		}
		this.blockLevel = this.activeInp ? 0 : 2;
		return document.getElementsByTagName('html')[0];
	}

	// eslint-disable-next-line class-methods-use-this
	askSettings() {
		chrome.runtime.sendMessage({
				"target": "background",
				"command": "getSettings"
			});
	}

	commandController(e) {
		for (const command in this.settings) {
			if (this.settings[command].code === e.code &&
				this.settings[command].ctrlKey === e.ctrlKey &&
				this.settings[command].altKey === e.altKey &&
				this.settings[command].shiftKey === e.shiftKey) {
					this.commands[command]();
			}
		}
	}

}

// eslint-disable-next-line init-declarations
var mainController;
if (!mainController) {
	mainController = new MainController();
}