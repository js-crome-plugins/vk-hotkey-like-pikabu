/* global chrome */

chrome.runtime.sendMessage({
	"target": "background",
	"command": "getSettings"
});

chrome.runtime.onMessage.addListener((message) => {
	switch (message.command) {
		case "getSettings":
			feelBattons(message.settings);
			break;
		default:
			console.error(message, "command is not find");
	}
});

window.onblur = () => {
	apply();
}

document.addEventListener('click', apply);

var handler = '';
var sample = {};

function removeListeners() {
	document.removeEventListener('keydown', handler);
	handler = '';
}

function feelBattons(obj) {
	for (const command in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, command)) {
			const batton = document.getElementById(command);
			batton.querySelector('.first-name').textContent = obj[command].code.replace('Key', "");
			let secondName = obj[command].ctrlKey ? 'ctrl' : "";
			secondName += obj[command].shiftKey ? ' shift' : "";
			secondName += obj[command].altKey ? ' alt' : "";
			batton.querySelector('.second-name').textContent = secondName;
			batton.addEventListener('click', onClickBtn);
		}
	}
}

function onClickBtn(event) {
	event.stopPropagation();
	apply();
	event.currentTarget.classList.add('editing');
	handler.length && removeListeners();
	handler = changeSettings.bind(this, event.currentTarget);
	document.addEventListener('keydown', handler);
}

function changeSettings(target, event) {
	const exept = [
		'ShiftLeft',
		'ControlLeft',
		'AltLeft',
		'ShiftRight',
		'ControlRight',
		'AltRight'
	];
	event.stopPropagation();
	event.preventDefault();
	if (!exept.includes(event.code)) {
		const keyName = event.code.replace('Key', "");
		const firstName = target.querySelector('.first-name');
		switch (keyName.length) {
			case 5:
				firstName.style.fontSize = "14px";
			break;
			case 6:
				firstName.style.fontSize = "12px";
			break;
			case 7:
				firstName.style.fontSize = "10px";
			break;
			case 8:
				firstName.style.fontSize = "8px";
			break;
			default:
				if (keyName.length < 5) {
					firstName.style.fontSize = "inherit"
				} else {
					firstName.style.fontSize = "8px";
				}
		}
		firstName.textContent = keyName;
		let secondName = event.ctrlKey ? 'ctrl' : "";
		secondName += event.shiftKey ? ' shift' : "";
		secondName += event.altKey ? ' alt' : "";
		target.querySelector('.second-name').textContent = secondName;
		sample[target.id] = {
			'code': event.code,
			'ctrlKey': event.ctrlKey,
			'shiftKey': event.shiftKey,
			'altKey': event.altKey
		}
	}
}

function apply() {
	if (JSON.stringify(sample) !== '{}') {
		chrome.runtime.sendMessage({
			"target": "background",
			"command": "applySettings",
			"args": sample
		});
		sample = {};
	}
	for (const elem of document.querySelectorAll('.editing')) {
		elem.classList.remove('editing');
	}
}