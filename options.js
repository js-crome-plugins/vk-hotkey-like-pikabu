/*global chrome */
document.getElementById('clear').onclick = () => {
	chrome.storage.sync.clear(function () {
		chrome.runtime.sendMessage({
			"target": "background",
			"command": "applySettings",
			"args": {}
		}, function() {
			console.log("DONE!");
		});
	});
}